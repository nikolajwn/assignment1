Sets
*Below I have defined the sets we are going to work with. The set K is the keys available, and
*the set N represents our nodes.
k keys /k1*k30/
n nodes /n1*n8/;

*Since we have to find direct connections between two available nodes, we will have to desribe two different nodes
*at the same time. This is why we now use the alias-command, which copies node n1*n8, and denotes them by s.
alias(n,s);

*The key management problem we need to solve, has the following characteristics. q describes the minimum number of
*keys two nodes must share to establish a direct connection. T indicates the maximum of how many times a single
*key can be used. m says that a single key occupies 186kB, and L describes the space limit for a single node. 
Parameters
q Restriction on minimum number of keys to share /3/
T max number of times key k can be used /2/
m space key k occupies /186/
L space limit for node n /1000/;

*To solve the problem we will need to define some variables. First of all, we define three binary variables. 
*The y-variable indicates if node n has key k (1), or not (0). The x-variable indicates if node n and s share
*key k (1), or not (0). delta(n,s) indicates if node n and s are directly connected (1), or not (0). 
Binary variable
y(n,k) indicates if node n has key k
x(n,s,k) indicates if node n and s share key k
delta(n,s) indicates if node n and s are directly connected;

*The idea of this problem is to maximize the total number of direct connections between pairs of nodes.
*This is done by selecting the right keys for the right nodes. We define the objective function value
*by the greek letter xi. 
Variable
xi objective function value;

*By now we can write down the mathematical formulation of our problem. We begin by giving names to
*the objective function and our constraints, and we furthermore indicate what they depend on.
Equations
objectivefunction the objective function
maximumkeystohold(n) Constraint specifying the maximum amount of keys a single node can hold
maximumreusesofkey(k) Constraint indicating the maximal number of times a key can be used
forcingx1(n,s,k) first constraint on x forcing it to take the values we want
forcingdeltas(n,s) constraint on delta forcing it to take the values we want;

*The objective function indicates that we would like to maximize the total number of direct connections between
*pairs of nodes. We sum over n and s>n, so we do not count every direct connection twice. Implicitly this means that
*delta(n,s) and delta(s,n) only count as one direct connection, and not two.
objectivefunction .. xi =e= sum(n, sum(s$(ord(s)>ord(n)),delta(n,s)));

*The first constraint implies that a node cannot hold more keys, than it's memory allows. In our case this is a
*maximum of five keys. A node can have everything between 0 and 5 keys.
maximumkeystohold(n) .. sum(k, y(n,k))*m =l= L;

*The second constraint implies that a key cannot be used more than two times. A key can be used between 0 and 2
*times.
maximumreusesofkey(k) .. sum(n, y(n,k)) =l= T;

*The third constraint forces x(n,s,k) to be zero, whenever node n and s do not share a single key. When they
*share a key, this constraint does not force x(n,s,k) to be one, but adding a constraint
*for this will not matter, since it is a maximization problem, where we want to maximize the sum of the
*delta(n,s)'s. x(n,s,k) will be one, whenever it can be.
forcingx1(n,s,k)$(ord(s)>ord(n)) .. 2*x(n,s,k)-y(n,k) =l= y(s,k);

*The fourth constraint forces delta(n,s) to be zero, whenever two notes n and s do not share 3 or more keys.
*It does not force it to be one, when n and s actually do share 3 or more keys, but adding this constraint
*will not matter since it is a maximization problem, where we want to maximize the delta(n,s)'s. delta(n,s)
*will be one, whenever it can be.
forcingdeltas(n,s)$(ord(s)>ord(n)) .. sum(k, x(n,s,k)) =g= q*delta(n,s);

*We define the maximization problem by all the previous descriptions.
Model keymanagementproblem /all/;

*We solve the optimization problem by indicating that we need to maximize xi under the given constraints.
Solve keymanagementproblem using MIP maximizing xi;

Display xi.l, y.l, x.l, delta.l;
*By the output we can see that the maximal number of direct connections is 4. 

